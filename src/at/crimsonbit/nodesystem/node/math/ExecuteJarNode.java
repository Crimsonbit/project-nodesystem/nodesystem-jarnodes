package at.crimsonbit.nodesystem.node.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import at.crimsonbit.nodesystem.frontend.gui.dialog.ExceptionDialog;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class ExecuteJarNode extends AbstractNode {

	private BufferedReader error;
	private BufferedReader op;
	private int exitVal;

	@NodeInput
	@NodeField
	String jarLocation;

	@NodeInput
	List<String> args;

	@NodeOutput
	Integer exitValue;

	public ExecuteJarNode() {
	}
	
	@Override
	public void compute() {
		executeJar(jarLocation, args);
	}

	public void executeJar(String jarFilePath, List<String> args) {
		// Create run arguments for the

		final List<String> actualArgs = new ArrayList<String>();
		actualArgs.add(0, "java");
		actualArgs.add(1, "-jar");
		actualArgs.add(2, jarFilePath);
		actualArgs.addAll(args);
		try {
			final Runtime re = Runtime.getRuntime();
			// final Process command = re.exec(cmdString, args.toArray(new String[0]));
			final Process command = re.exec(actualArgs.toArray(new String[0]));
			this.error = new BufferedReader(new InputStreamReader(command.getErrorStream()));
			this.op = new BufferedReader(new InputStreamReader(command.getInputStream()));
			// Wait for the application to Finish
			command.waitFor();
			exitValue = command.exitValue();
			if (exitValue != 0) {
				throw new IOException("Failed to execure jar, " + this.getExecutionLog());
			}

		} catch (final IOException | InterruptedException e) {
			new ExceptionDialog(e);
		}
	}

	public String getExecutionLog() {
		String error = "";
		String line;
		try {
			while ((line = this.error.readLine()) != null) {
				error = error + "\n" + line;
			}
		} catch (final IOException e) {
		}
		String output = "";
		try {
			while ((line = this.op.readLine()) != null) {
				output = output + "\n" + line;
			}
		} catch (final IOException e) {
		}
		try {
			this.error.close();
			this.op.close();
		} catch (final IOException e) {
		}
		return "exitVal: " + this.exitVal + ", error: " + error + ", output: " + output;
	}

}
