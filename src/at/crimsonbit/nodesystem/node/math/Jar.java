package at.crimsonbit.nodesystem.node.math;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

/**
 *
 * @author Florian Wagner
 *
 */
public enum Jar implements INodeType {

	EXECUTE_JAR("Execute Jar"), ARGS_LIST("Arguments"), APPEN_ARGS("Append Argument");
	
	private String name;
	
	Jar(String n){
		this.name= n;
	}
	
	@Override
	public String nodeName() {
		return name;
	}
	
}
