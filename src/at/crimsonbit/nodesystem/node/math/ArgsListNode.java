package at.crimsonbit.nodesystem.node.math;

import java.util.ArrayList;
import java.util.List;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class ArgsListNode extends AbstractNode{
	
	@NodeOutput
	List<String> argsList;
	
	@Override
	public void compute() {
		argsList = new ArrayList<String>();
	}

}
