package at.crimsonbit.nodesystem.node.math;

import java.util.ArrayList;
import java.util.List;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class ArgsAppenderNode extends AbstractNode {

	@NodeInput
	List<String> argsList;

	@NodeInput
	String arg;

	@NodeOutput
	List<String> output;

	@Override
	public void compute() {
		if (argsList != null && arg != null) {
			argsList.add(arg);
			output = argsList;
		} else {
			output = new ArrayList<String>();
		}
	}

}
