package at.crimsonbit.nodesystem.node.math;

import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class JarNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(Jar.APPEN_ARGS, ArgsAppenderNode.class);
		registry.registerDefaultFactory(Jar.ARGS_LIST, ArgsListNode.class);
		registry.registerDefaultFactory(Jar.EXECUTE_JAR, ExecuteJarNode.class);
	}

}
